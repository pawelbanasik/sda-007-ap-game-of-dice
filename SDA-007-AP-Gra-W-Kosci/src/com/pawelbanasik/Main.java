package com.pawelbanasik;

import java.util.Random;

public class Main {

	public static void main(String[] args) {

		Random dice = new Random();

		int resultA;
		int resultB;

		do {
			resultA = dice.nextInt(6) + 1;
			resultB = dice.nextInt(6) + 1;
			System.out.println("Dice A: " + resultA + ", Dice B: " + resultB);
		} while (resultA != 6 || resultB != 6);
	}

}
